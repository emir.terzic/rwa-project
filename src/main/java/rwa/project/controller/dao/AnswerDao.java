package rwa.project.controller.dao;

import javax.persistence.EntityManager;

import rwa.project.model.Answer;

public class AnswerDao extends AbstractDao {

	public AnswerDao() {
		super();
	}

	public void save(Answer answer) {
		EntityManager em = createEntityManager();
		em.getTransaction().begin();
		em.persist(answer);
		em.getTransaction().commit();
		em.close();	
	}
}
