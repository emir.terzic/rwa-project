package rwa.project.controller.dao;

import javax.persistence.EntityManager;

import rwa.project.model.Question;

public class QuestionDao extends AbstractDao {

	public QuestionDao() {
		super();
	}

	public void save(Question question) {
		EntityManager em = createEntityManager();
		em.getTransaction().begin();
		em.persist(question);
		em.getTransaction().commit();
		em.close();	
	}
}
