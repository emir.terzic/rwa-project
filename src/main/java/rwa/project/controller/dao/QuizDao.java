package rwa.project.controller.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import rwa.project.model.Quiz;

public class QuizDao extends AbstractDao {
	

	public QuizDao() {
		super();
	}
	
	public List<Quiz> findAll() {
		EntityManager em = createEntityManager();
		Query q = em.createQuery("SELECT q FROM Quiz q");
		List<Quiz> resultList = q.getResultList();
		em.close();
		return resultList;
	}
	
	public List<Quiz> getNQuizzes(int n, int s) {
		EntityManager em = createEntityManager();
		em.getTransaction().begin();
		Query q = em.createQuery("SELECT q FROM Quiz q");
		q.setFirstResult(s);
		q.setMaxResults(n);
		List<Quiz> quizList = q.getResultList();
		em.getTransaction().commit();
		em.close();
		return quizList;
	}
	
	public void save(Quiz quiz) {
		EntityManager em = createEntityManager();
		em.getTransaction().begin();
		em.persist(quiz);
		em.getTransaction().commit();
		em.close();	
	}
}
