package rwa.project.controller.service;

import rwa.project.controller.dao.AnswerDao;
import rwa.project.model.Answer;

public class AnswerService {
	
	private AnswerDao answerDao;
	
	public AnswerService(AnswerDao answerDao) {
		this.answerDao = answerDao;
	}
	
	public void create(Answer answer) {
		answerDao.save(answer);	
	}	
}
