package rwa.project.controller.service;

import java.util.ArrayList;
import java.util.List;

import rwa.project.controller.dao.QuizDao;
import rwa.project.model.Quiz;

public class QuizService {
	
	private QuizDao quizDao;
	
	public QuizService(QuizDao quizDao) {
		this.quizDao = quizDao;
	}
	
	public void save(Quiz quiz) {
		quizDao.save(quiz);
	}
	


	public List<Quiz> getNQuizzes(int count, int skip) {
		return quizDao.getNQuizzes(count, skip);
	}
}
