package rwa.project.controller.service;

import java.util.List;

import rwa.project.controller.dao.UserDao;
import rwa.project.model.User;
import rwa.project.util.SecurityUtil;

public class UserService {
	
	private UserDao userDao;
	
	public UserService(UserDao userDao) {
		this.userDao = userDao;
	}
	
	public void create(User user) {
		user.setPassword(SecurityUtil.hashPassword(user.getPassword()));
		userDao.save(user);	
	}
	
	public List<User> findAll() {
		return userDao.findAll();
	}
	
	public User findByUsername(String username) {
		return userDao.findByUsername(username);
	}
	
	public User authenticate(String username, String password) {
		
		User user = findByUsername(username);
		
		if (user == null) {
			return null;
		}
		
/*		if (SecurityUtil.checkPassword(password, user.getPassword())) {
			user.setPassword("");
			return user;
		}
*/		
		return user;
	}
}
