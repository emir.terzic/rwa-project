package rwa.project.controller.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import rwa.project.controller.dao.QuizDao;
import rwa.project.controller.service.QuizService;
import rwa.project.model.Quiz;

/**
 * Servlet implementation class LoadQuizAdmin
 */
public class LoadQuizAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoadQuizAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");
	    response.setCharacterEncoding("utf-8");
	    QuizDao quizDao = new QuizDao();
	    QuizService quizService = new QuizService(quizDao);
//	    int count = Integer.parseInt(request.getParameter("count"));
//	    int skip = Integer.parseInt(request.getParameter("skip"));
	    List<Quiz> quizzes = quizService.getNQuizzes(10, 0);
	    
	    Gson gson = new Gson();
	    String jsonData;
	    
	    jsonData = gson.toJson(quizzes);
	    response.getWriter().print(jsonData);
	    System.out.println(jsonData);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
