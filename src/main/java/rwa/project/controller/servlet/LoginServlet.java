package rwa.project.controller.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import rwa.project.controller.dao.UserDao;
import rwa.project.controller.service.UserService;
import rwa.project.model.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/Login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public LoginServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		UserService userService = new UserService(new UserDao());
		User user = userService.authenticate(username, password);
		if(user != null) {
		//	Gson gson = new Gson();
		//	String jsonData = gson.toJson(user);
		//	HttpSession session = request.getSession(true);
		//	session.setAttribute("currentUser", user);
		//	System.out.println(request.getContextPath());
		//	RequestDispatcher rs = request.getRequestDispatcher("super-admin.html");
        //    rs.include(request, response);
			System.out.println("Logged in");
			response.sendRedirect("admin/super-admin.html");		
		return;	
		}
	
	}

}
