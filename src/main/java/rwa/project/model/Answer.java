package rwa.project.model;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;
import javax.servlet.http.HttpServletRequest;

import rwa.project.controller.dao.AnswerDao;

/**
 * Entity implementation class for Entity: Answer
 *
 */
@Entity(name = "Answer")
public class Answer implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@ManyToOne(fetch = FetchType.LAZY)
	private Question question;
	private String content;
	private boolean correct;
	private static final long serialVersionUID = 1L;

	public Answer() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public Question getQuestion() {
		return this.question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}   
	public String getContent() {
		return this.content;
	}
	

	public void setContent(String content) {
		this.content = content;
	}   
	public boolean getCorrect() {
		return this.correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}
   
	public void createAnswer(HttpServletRequest request, int questionNumber, int answerNumber, Question parentQuestion) {
		this.setQuestion(parentQuestion);
		this.setContent(request.getParameter("qNa[" + questionNumber + "][answers]" + "[" + answerNumber + "][answer]"));
		this.setCorrect(Boolean.parseBoolean(request.getParameter("qNa[" + questionNumber + "][answers]" + "[" + answerNumber + "][state]")));
		parentQuestion.getAnswers().add(this);
	}
}
