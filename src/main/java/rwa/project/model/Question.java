package rwa.project.model;

import java.io.Serializable;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.servlet.http.HttpServletRequest;

import rwa.project.controller.dao.AnswerDao;
import rwa.project.controller.dao.QuestionDao;

/**
 * Entity implementation class for Entity: Question
 *
 */
@Entity(name = "Question")
public class Question implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String content;
	private Duration time;
	private int points;
	@ManyToOne(fetch = FetchType.LAZY)
	private Quiz quiz;
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	private Collection<Answer> answers;
	private static final long serialVersionUID = 1L;

	public Question() {
		super();
	}   
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}   
	public Duration getTime() {
		return this.time;
	}

	public void setTime(Duration time) {
		this.time = time;
	}   
	public int getPoints() {
		return this.points;
	}

	public void setPoints(int points) {
		this.points = points;
	}   
	public Quiz getQuiz_id() {
		return this.quiz;
	}

	public void setQuiz(Quiz quiz) {
		this.quiz = quiz;
	}   
	public Collection<Answer> getAnswers() {
		return this.answers;
	}

	public void assignAnswers(HttpServletRequest request, int questionNumber) {
		
		int answersCount = Integer.parseInt(request.getParameter("answerNumber[" +"]"));
		System.out.println(answersCount);
		answers = new ArrayList<Answer>();
		for(int i=0; i<answersCount; i++) {
			Answer answer = new Answer();
			answer.createAnswer(request, questionNumber, i, this);
		}
	}
	
	public void setAnswer(Collection<Answer> answers) {
		this.answers = answers;
	}
	public void createQuestion(HttpServletRequest request, int i, Quiz parentQuiz) {
		
		this.setQuiz(parentQuiz);
		this.setContent(request.getParameter("qNa[" + i + "][question]"));
		this.assignAnswers(request, i);
		parentQuiz.getQuestions().add(this);
	}    
}
