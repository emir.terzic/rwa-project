package rwa.project.model;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.*;
import javax.servlet.http.HttpServletRequest;

import rwa.project.controller.dao.QuizDao;
import rwa.project.controller.service.QuizService;

/**
 * Entity implementation class for Entity: Quiz
 *
 */
@Entity(name = "Quiz")
public class Quiz implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String name;
	private String description;
	@OneToOne
	private User creator;
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	private Collection<Question> questions;
	private boolean active;
	private String image;
	private static final long serialVersionUID = 1L;

	public Quiz() {
		super();
	}   
	public void createQuiz(HttpServletRequest request) {
		questions = new ArrayList<Question>();
		this.setName(request.getParameter("quizName"));
		this.setDescription(request.getParameter("quizDescription"));
		this.assignQuestions(request);
		QuizDao quizDao = new QuizDao();
		QuizService quizService = new QuizService(quizDao);
		quizService.save(this);
	}
	
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}   
	public User getCreator() {
		return this.creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}   
	public Collection<Question> getQuestions() {
		return this.questions;
	}

	public void setQuestions(Collection<Question> questions) {
		this.questions = questions;
	}   
	
	public void assignQuestions(HttpServletRequest request) {
		int questionsCount = Integer.parseInt(request.getParameter("questionNumber"));
		
		for( int i=0; i<questionsCount; i++) {
			Question question = new Question();
			question.createQuestion(request, i, this);
		}
	}
	
	public void addQuestion(Question question) {
		this.questions.add(question);
	}
	
	public boolean getActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}   
	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}
   
}
