//Displaying panels
	
function displayPanel(id){
	switch(id){
	case "quiz-editor":
		$("#user-editor").slideUp("fast");
		break;
		default:
			$("#quiz-editor").slideUp("fast");
		break;
	}
		$("#"+id).slideDown("slow");
}

function hideQuizPanel(){
	$("#quiz-editor").slideUp("fast");
	var boxes = document.getElementsByClassName("quiz-question-box");
	while(boxes.length>0){
		removeSegment(boxes[0].id);
	}
}
function hideUserPanel(){
	$("#user-editor").slideUp("fast");
}

var anCount = 0;
//Adding forms in Admin panels
function addAnswer(myId){
	var answerDiv = document.createElement("div");
	answerDiv.setAttribute("class","mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--7-col mdl-cell--7-col-tablet is-upgraded quiz-answer");
	answerDiv.setAttribute("data-upgraded",",MaterialTextfield");
	answerDiv.setAttribute("id","answer-"+anCount);
	var input = document.createElement("INPUT");
	input.setAttribute("type","text");
	input.classList.add("mdl-textfield__input");
	input.setAttribute("id","form-quiz-answer-"+anCount);
	input.setAttribute("placeholder","Odgovor...");
	answerDiv.appendChild(input);
	
	var checkBoxDiv = document.createElement("div");
	checkBoxDiv.setAttribute("class","mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--3-col mdl-cell--3-col-tablet is-upgraded quiz-answer-state");
	checkBoxDiv.setAttribute("data-upgraded",",MaterialTextfield");
	var cbLbl = document.createElement("LABEL");
	cbLbl.setAttribute("class","mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect");
	cbLbl.setAttribute("for","checkbox-"+anCount);
	checkBoxDiv.setAttribute("id","answer-cb-"+anCount);
	checkBoxDiv.appendChild(cbLbl);
	
	var cbInput = document.createElement("INPUT");
	cbInput.setAttribute("type","checkbox");
	cbInput.setAttribute("id","checkbox-"+anCount);
	cbInput.classList.add("mdl-checkbox__input");
	checkBoxDiv.appendChild(cbInput);
	
	var cbSpan = document.createElement("span");
	cbSpan.classList.add("mdl-checkbox__label");
	cbSpan.setAttribute("id","cbLabel-"+anCount);
	cbSpan.innerHTML = "Tačno";
	checkBoxDiv.appendChild(cbSpan);
	
	var deleteDiv = document.createElement("div");
	deleteDiv.setAttribute("class","mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--2-col mdl-cell--2-col-tablet is-upgraded");
	deleteDiv.setAttribute("id","answer-delete-"+anCount);
	var button = document.createElement("BUTTON");
	button.setAttribute("id","remove-answer-"+anCount);
	button.setAttribute("class","mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent custom-button-icon-remove");
	button.setAttribute("onclick","removeAnswer("+anCount+")");
	button.innerHTML = "<i class=' material-icons custom-icons'>delete</i>";
	deleteDiv.appendChild(button);
	
	$(answerDiv).insertBefore("#"+myId);
	$(checkBoxDiv).insertBefore("#"+myId);
	$(deleteDiv).insertBefore("#"+myId);
	anCount = anCount+1;
}

var qNum = 0;

function addQuestion(){
	var box = document.createElement("div");
	box.setAttribute("class","mdl-cell mdl-cell--12-col mdl-cell--12-col-tablet quiz-question-box ");
	box.setAttribute("id","question-number-"+qNum);
	var boxChild = document.createElement("div");
	boxChild.setAttribute("class","mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-cell mdl-cell--12-col mdl-cell--12-col-tablet");
	
	var input = document.createElement("INPUT");
	input.setAttribute("type","text");
	input.classList.add("mdl-textfield__input");
	input.setAttribute("id","form-quiz-question");
	input.setAttribute("placeholder","Pitanje...");
	boxChild.appendChild(input);
	box.appendChild(boxChild);
	
	var button = document.createElement("BUTTON");
	button.setAttribute("class","mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent custom-submit");
	button.setAttribute("id","add-answer-button-"+qNum);
	button.setAttribute("onclick","addAnswer('add-answer-button-"+qNum+"')");
	button.innerHTML = "<i class='material-icons  custom-icons' style='margin-right:10px'>add</i> Dodaj odgovor";
	box.appendChild(button);
	
	button = document.createElement("BUTTON");
	button.setAttribute("class","mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent custom-submit-alt");
	button.setAttribute("id","delete-answer-button-"+qNum);
	button.setAttribute("onclick","removeSegment('question-number-"+qNum+"')");
	button.innerHTML = "<i class='material-icons  custom-icons' style='margin-right:10px'>delete</i> Izbriši pitanje";
	box.appendChild(button);
	qNum=qNum+1;
	
	$(box).insertBefore("#add-question-button");
	
}

function removeSegment(id){
	$("#"+id).remove();
}

function removeAnswer(num){
	$("#answer-"+num).remove();
	$("#answer-cb-"+num).remove();
	$("#answer-delete-"+num).remove();
}


//SENDING DATA TO SERVER

function createUser(){
	var username = document.getElementById("form-user-name").value;
	var fullname = document.getElementById("form-user-full-name").value;
	var password = document.getElementById("form-user-password").value;
	var role = document.getElementById("super-user-chbox").checked;
	
	$.ajax({
		type:'POST',
		data:{username:username,fullname:fullname,password:password,role:role},
		url:"/rwa-project/CreateUser"
	});
}

function createQuiz(){
	var quizName = document.getElementById("form-quiz-name").value;
	var quizDescription = document.getElementById("form-quiz-description").value;
	var quizImageLink = document.getElementById("form-quiz-image").value;
	var questionList = document.getElementsByClassName("quiz-question-box");
	var collectionList = [];
	var questionCount = 0;
	var answersPerQuestion = [];
	
	while(questionList.length>0){
		var answerList = questionList[0].getElementsByClassName("quiz-answer");
		var answerListState = questionList[0].getElementsByClassName("quiz-answer-state");
		var answerCollection = [];
		questionCount += 1;
		answersCount = 0;
		while(answerList.length>0){
			var question = {};
			var answer = answerList[0].firstChild;
			answerListState[0].firstChild.remove();
			var state = answerListState[0].firstChild;
			question.answer = answer.value;
			question.state = state.checked;
			answerCollection.push(question);
			answerList[0].remove();
			answerListState[0].remove();
			answersCount += 	1;
		}
		var question = questionList[0].firstChild.firstChild.value;
		questionList[0].remove();
		var qNa = {};
		qNa.question = question;
		qNa.answers = answerCollection;
		collectionList.push(qNa);
		answersPerQuestion.push(answersCount);
	}
	
	$.ajax({
		type:'POST',
		data:{quizName:quizName,quizDescription:quizDescription,quizImage:quizImageLink,qNa:collectionList,answerNumber:answersPerQuestion, questionNumber:questionCount},
		url:"/rwa-project/CreateQuiz", 
		success: function(data,status){
			//location.reload();
		}
	});
}
